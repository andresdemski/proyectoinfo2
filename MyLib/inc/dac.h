/*
 * dac.h
 *
 *  Created on: 22/10/2013
 *      Author: andresdemski
 */

#ifndef DAC_H_
#define DAC_H_

void Init_Dac (void);
void Write_Dac (uint16_t val);


#endif /* DAC_H_ */
