/*
 * delay.h
 *
 *  Created on: 20/08/2013
 *      Author: Andres
 */

#ifndef DELAY_H_
#define DELAY_H_

void msDelay (uint32_t ms);

#endif /* DELAY_H_ */
