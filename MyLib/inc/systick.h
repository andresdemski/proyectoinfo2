/*
 * systick.h
 *
 *  Created on: 14/08/2013
 *      Author: Andres
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

void SysTick_ClrIrq (void);
void SysTick_Enable ();

#endif /* SYSTICK_H_ */
