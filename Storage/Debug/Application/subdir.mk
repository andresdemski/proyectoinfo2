################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/lpc17xx_clkpwr.c \
../Application/lpc17xx_i2c.c \
../Application/lpc17xx_pinsel.c \
../Application/lpc17xx_uart.c \
../Application/retarget.c 

OBJS += \
./Application/lpc17xx_clkpwr.o \
./Application/lpc17xx_i2c.o \
./Application/lpc17xx_pinsel.o \
./Application/lpc17xx_uart.o \
./Application/retarget.o 

C_DEPS += \
./Application/lpc17xx_clkpwr.d \
./Application/lpc17xx_i2c.d \
./Application/lpc17xx_pinsel.d \
./Application/lpc17xx_uart.d \
./Application/retarget.d 


# Each subdirectory must supply rules for building sources it contributes
Application/%.o: ../Application/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DNDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"/home/andresdemski/git/ProyectoInfo2/Storage/Application" -I"/home/andresdemski/git/ProyectoInfo2/CMSISv2p00_LPC17xx/inc" -I"/home/andresdemski/git/ProyectoInfo2/Storage/FatFs" -I"/home/andresdemski/git/ProyectoInfo2/Storage/FatFs/option" -I"/home/andresdemski/git/ProyectoInfo2/Storage/SD" -I"/home/andresdemski/git/ProyectoInfo2/Storage/USBHost" -I"/home/andresdemski/git/ProyectoInfo2/Storage/USBHost/Host" -I"/home/andresdemski/git/ProyectoInfo2/Storage/USBHost/Include" -I"/home/andresdemski/git/ProyectoInfo2/Storage/USBHost/MassStorage" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


