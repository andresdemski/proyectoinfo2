################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/TFT.c \
../src/Touch.c \
../src/font.c 

OBJS += \
./src/TFT.o \
./src/Touch.o \
./src/font.o 

C_DEPS += \
./src/TFT.d \
./src/Touch.d \
./src/font.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DDEBUG -D__CODE_RED -I"/home/andresdemski/git/ProyectoInfo2/MyLib/inc" -I"/home/andresdemski/git/ProyectoInfo2/TFT/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


