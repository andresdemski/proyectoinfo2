/*
 * TFT.h
 *
 *  Created on: 27/08/2013
 *      Author: Andres
 */

#ifndef TFT_H_
#define TFT_H_

////////////////DISPLAY/////////////////////

#define			TFT_DB0				LCD_D4
#define			TFT_DB1				LCD_D5
#define			TFT_DB2				LCD_D6
#define			TFT_DB3				LCD_D7
#define			TFT_DB4				Expantion12
#define			TFT_DB5				Expantion13
#define			TFT_DB6				Expantion14
#define			TFT_DB7				Expantion15
#define			TFT_DB8				Expantion1
#define			TFT_DB9				Expantion2
#define			TFT_DB10			Expantion3
#define			TFT_DB11			Expantion4
#define			TFT_DB12			Expantion6
#define			TFT_DB13			Expantion7
#define			TFT_DB14			Expantion8
#define			TFT_DB15			Expantion9
#define			TFT_RS				LCD_RST
#define			TFT_WR				LCD_E
#define			TFT_RESET			Expantion11

/////////////////////MASKS///////////////////
#define			MASK_BLUE_1			(1<<27)
#define			MASK_GREEN_1		(1<<23)
#define			MASK_RED_1			(9<<19)

#define			MASK_BLUE_2			(1<<5)
#define			MASK_GREEN_4		(1<<28)


//////////////////FONT CONTROL//////////////
#define			SMALL_FONT			0
#define			BIG_FONT			1

#define			TAMANIO_X(x)		(8+x*8)
#define			TAMANIO_Y(x)		(12+x*4)

#define			BLACK				0
#define			WHITE				1
#define			RED					2
#define			BLUE				3
#define			GREEN				4



extern 	uint8_t Small_Font[];
extern 	uint8_t Big_Font[];

typedef struct {
	uint8_t Red;
	uint8_t Green;
	uint8_t Blue;
}Pixel_s;

extern __RW uint32_t TFT_DB [16][2];

void Init_TFT (void);
void TFT_Write_Com_Data (uint16_t Com, uint16_t Dat);
void TFT_Write_Com (uint16_t Com);
void TFT_Write_Data (uint16_t Dat);
void TFT_Set_Address (uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
uint16_t TFT_Color (uint8_t r, uint8_t g, uint8_t b);
void TFT_Screen_Color (uint8_t r, uint8_t g, uint8_t b); // bits: [7:3] r  [7:2] g [7:3] bl
void TFT_Send_Color (uint16_t color);
void TFT_Three_Lines ( void );
void TFT_Print (uint8_t *stg, uint8_t x, uint16_t y,uint8_t tamanio,uint8_t color);
void TFT_PrintChar (uint8_t CHAR, uint8_t x,uint16_t y,uint8_t tamanio,uint8_t color);

#endif /* TFT_H_ */
