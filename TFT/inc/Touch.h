/*
 * Touch.h
 *
 *  Created on: 18/09/2013
 *      Author: andresdemski
 */

#ifndef TOUCH_H_
#define TOUCH_H_

//////////////////TOUCH////////////////////////

#define			TOUCH_CS			Expantion16
#define			TOUCH_CLK			SCK
#define			TOUCH_IN			MISO
#define			TOUCH_BUSY			Expantion17
#define			TOUCH_OUT			MOSI
#define			TOUCH_PENIRQ		Expantion5

typedef struct
{
	uint16_t X;
	uint16_t Y;
} TouchCord_;

void Init_Touch (void);
void Get_Touch (void);

extern TouchCord_ TouchXY;
#endif /* TOUCH_H_ */
