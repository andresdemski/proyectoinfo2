/*
 * Init.c
 *
 *  Created on: 27/08/2013
 *      Author: Andres
 */

#include "MyLPC1769.h"
#include "InfotronicKit.h"
#include "port.h"
#include "delay.h"
#include "TFT.h"
#include <string.h>

__RW uint32_t DB [16] [2] =
{
		{TFT_DB0} ,
		{TFT_DB1} ,
		{TFT_DB2} ,
		{TFT_DB3} ,
		{TFT_DB4} ,
		{TFT_DB5} ,
		{TFT_DB6} ,
		{TFT_DB7} ,
		{TFT_DB8} ,
		{TFT_DB9} ,
		{TFT_DB10} ,
		{TFT_DB11} ,
		{TFT_DB12} ,
		{TFT_DB13} ,
		{TFT_DB14} ,
		{TFT_DB15}
};

/**
 * \fn void Init_TFT (void)
 * \brief Inicializa la pantalla
 * \details Inicializa la pantalla
 */
void Init_TFT (void)
{
	uint8_t i=0;

	for (i=0;i<16; i++)
	{
		SetPinSel(DB[i][0],DB[i][1],0);
		SetPinDir(DB[i][0],DB[i][1],SALIDA);
		SetPinModeOD(DB[i][0],DB[i][1],O_NORMAL);
		ClrPin(DB[i][0],DB[i][1]);
	}

	SetPinSel(TFT_RS,0);
	SetPinDir(TFT_RS,SALIDA);
	SetPinModeOD(TFT_RS,O_NORMAL);
	ClrPin(TFT_RS);

	SetPinSel(TFT_RESET,0);
	SetPinDir(TFT_RESET,SALIDA);
	SetPinModeOD(TFT_RESET,O_NORMAL);
	SetPin(TFT_RESET);

	SetPinSel(TFT_WR,0);
	SetPinDir(TFT_WR,SALIDA);
	SetPinModeOD(TFT_WR,O_NORMAL);
	SetPin (TFT_WR);

	msDelay(200);

    SetPin (TFT_RESET);
    msDelay(50);
	ClrPin (TFT_RESET);
	msDelay(50);
	SetPin (TFT_RESET);

	msDelay(100);

	TFT_Write_Com_Data(0x0000,0x0001);    msDelay(1);
    TFT_Write_Com_Data(0x0003,0xA8A4);    msDelay(1);
    TFT_Write_Com_Data(0x000C,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x000D,0x080C);    msDelay(1);
    TFT_Write_Com_Data(0x000E,0x2B00);    msDelay(1);
    TFT_Write_Com_Data(0x001E,0x00B7);    msDelay(1);
    TFT_Write_Com_Data(0x0001,0x2B3F);    msDelay(1);
    TFT_Write_Com_Data(0x0002,0x0600);    msDelay(1);
    TFT_Write_Com_Data(0x0010,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0011,0x6070);    msDelay(1);
    TFT_Write_Com_Data(0x0005,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0006,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0016,0xEF1C);    msDelay(1);
    TFT_Write_Com_Data(0x0017,0x0003);    msDelay(1);
    TFT_Write_Com_Data(0x0007,0x0233);    msDelay(1);
    TFT_Write_Com_Data(0x000B,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x000F,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0041,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0042,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0048,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0049,0x013F);    msDelay(1);
    TFT_Write_Com_Data(0x004A,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x004B,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0044,0xEF00);    msDelay(1);
    TFT_Write_Com_Data(0x0045,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0046,0x013F);    msDelay(1);
    TFT_Write_Com_Data(0x0030,0x0707);    msDelay(1);
    TFT_Write_Com_Data(0x0031,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0032,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0033,0x0502);    msDelay(1);
    TFT_Write_Com_Data(0x0034,0x0507);    msDelay(1);
    TFT_Write_Com_Data(0x0035,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0036,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0037,0x0502);    msDelay(1);
    TFT_Write_Com_Data(0x003A,0x0302);    msDelay(1);
    TFT_Write_Com_Data(0x003B,0x0302);    msDelay(1);
    TFT_Write_Com_Data(0x0023,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0024,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0025,0x8000);    msDelay(1);
    TFT_Write_Com_Data(0x004f,0);    msDelay(1);
    TFT_Write_Com_Data(0x004e,0);    msDelay(1);
    TFT_Write_Com(0x22);    msDelay(1);

}

/**
 * \fn void TFT_Write_Com_Data (uint16_t Com, uint16_t Dat)
 * \brief Envia un comando y un dato
 * \details Envia un comando y un dato
 */
void TFT_Write_Com_Data (uint16_t Com, uint16_t Dat)
{
	TFT_Write_Com (Com);
	TFT_Write_Data (Dat);

}

/**
 * \fn void TFT_Write_Com (uint16_t Com)
 * \brief Envia un comando
 * \details Envia un comando
 */
void TFT_Write_Com (uint16_t Com)
{
	uint8_t i=0;
	ClrPin(TFT_RS);    //Comando
	for (i=0;i<16;i++)
	{
		if ( (Com>>i) & 1 )		SetPin(DB[i][0],DB[i][1]);
		else	ClrPin(DB[i][0],DB[i][1]);
	}
	ClrPin (TFT_WR);
	SetPin (TFT_WR);
}

/**
 * \fn void TFT_Write_Data (uint16_t Dat)
 * \brief Envia un dato
 * \details Envia un dato
 */
void TFT_Write_Data (uint16_t Dat)
{
	uint8_t i=0;
	SetPin(TFT_RS);    //Dato
	for (i=0;i<16;i++)
	{
		if ( (Dat>>i) & 1 )			SetPin(DB[i][0],DB[i][1]);
		else	ClrPin(DB[i][0],DB[i][1]);
	}
	ClrPin (TFT_WR);
	SetPin (TFT_WR);
}

/**
 * \fn void TFT_Set_Address (uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
 * \brief Setea el lugar de escritura en la pantalla
 * \details Setea el lugar de escritura en la pantalla
 */
void TFT_Set_Address (uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	TFT_Write_Com_Data(0x0044,(x2<<8)|x1);
	TFT_Write_Com_Data(0x0045,y1);
	TFT_Write_Com_Data(0x0046,y2);
	TFT_Write_Com_Data(0x004e,x1);
	TFT_Write_Com_Data(0x004f,y1);
    TFT_Write_Com(0x0022);
}

/**
 * \fn uint16_t TFT_Color (uint8_t r, uint8_t g, uint8_t b)
 * \brief Convierte el RGB en un dato para poder enviarlo
 * \details Convierte el RGB en un dato para poder enviarlo
 */
uint16_t TFT_Color (uint8_t r, uint8_t g, uint8_t b)
{
	uint16_t color=0;
	r = r >> 3 ;
	g = g >> 2 ;
	b = b >> 3 ;
	color |= ( ( ( ( (color | r) << 6 ) | g ) << 5 ) | b);
	return color;
}

/**
 * \fn void TFT_Send_Color (uint16_t color)
 * \brief Envia el color
 * \details Envia el color
 */
void TFT_Send_Color (uint16_t color)
{
	SetPin(TFT_RS);    //Dato

	if ( color & (1<<4) ) {	SetPin(TFT_DB4); SetPin (TFT_DB3); }
		else	{ ClrPin(TFT_DB4); ClrPin (TFT_DB3); }
	if ( color & (1<<10) )	{SetPin(TFT_DB10); SetPin (TFT_DB9); }
		else	{ ClrPin(TFT_DB10); ClrPin (TFT_DB9); }
	if ( color & (1<<15) )	{ SetPin(TFT_DB15); SetPin (TFT_DB14); }
		else	{ ClrPin(TFT_DB15); ClrPin (TFT_DB14); }

	ClrPin (TFT_WR);
	SetPin (TFT_WR);
}


/*   Version funcional pero lenta
void TFT_Screen_Color (uint8_t r, uint8_t g, uint8_t b) // bits: [7:3] r  [7:2] g [7:3] b
{
	int i,j,k;
	uint16_t color= TFT_Color(r,g,b);

	TFT_Set_Address(0,0,239,319);

	for (k=0;k<16;k++)
	{
		ClrPin(DB[k][0],DB[k][1]);
	}

    for(i=0;i<320;i++)
    {
	  for (j=0;j<240;j++)
	  {
			TFT_Send_Color(color);
	  }
    }
}
*/

/**
 * \fn void TFT_Screen_Color (uint8_t r, uint8_t g, uint8_t b)
 * \brief Cambia el color de la pantalla
 * \details El RGB debe ser booleano
 */
void TFT_Screen_Color (uint8_t r, uint8_t g, uint8_t b) // r g y b deben ser bool
{

	///Esta funcion esta hecha tocando los registros del Cortex solo por cuestiones de optimizacion ya que tardaba mas de 2 segundos en refrescar la pantalla
	int i,j,k;

	TFT_Set_Address(0,0,239,319);

	for (k=0;k<16;k++)
	{
		ClrPin(DB[k][0],DB[k][1]);
	}

	SetPin(TFT_RS);

	for(i=0;i<320;i++)
    {
	  for (j=0;j<240;j++)
	  {
		FIOCLR1	= (MASK_RED_1 | MASK_GREEN_1 | MASK_BLUE_1);
		FIOCLR2 = (MASK_BLUE_2);
		FIOCLR4 = (MASK_GREEN_4);

		FIOSET1 = (r * MASK_RED_1) | (g * MASK_GREEN_1) | (b * MASK_BLUE_1);
		FIOSET2 = b * MASK_BLUE_2;
		FIOSET4 = g * MASK_GREEN_4;

		ClrPin (TFT_WR);
		SetPin (TFT_WR);
	  }
    }
}
/*
void TFT_PrintChar (uint8_t CHAR, uint8_t x,uint8_t y,uint8_t tamanio,uint8_t color)
{
	uint8_t i,j,k,*ch;

	TFT_Set_Address(x,y, x + TAMANIO_X(tamanio)-1, y+ TAMANIO_Y(tamanio)-1 );
	TFT_Set_Address(x,y, x + TAMANIO_X(tamanio)-1, y+ TAMANIO_Y(tamanio)-1 );

	ch= (tamanio)?Big_Font:Small_Font ;
	ch += (CHAR-'!'+1)*12;

	for (k=0;k<16;k++)
	{
		ClrPin(DB[k][0],DB[k][1]);
	}

	SetPin(TFT_RS);

	if (color)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for(i=0;i<(TAMANIO_X(tamanio));i++)
			{
				//if( (*ch) & ( 1<< (7-i) ) )
				//{
					FIOSET1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
					FIOSET2 = MASK_BLUE_2;
					FIOSET4 = MASK_GREEN_4;

				//}

				else
				{
					FIOCLR1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
					FIOCLR2 = MASK_BLUE_2;
					FIOCLR4 = MASK_GREEN_4;
				}

				ClrPin (TFT_WR);
				SetPin (TFT_WR);

			}
			ch++;
		}
	}

	else
	{
			for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{
					if( (*ch) & ( 1<< (7-i) ) )
					{
						FIOCLR1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
						FIOCLR2 = MASK_BLUE_2;
						FIOCLR4 = MASK_GREEN_4;

					}
					else
					{
						FIOSET1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
						FIOSET2 = MASK_BLUE_2;
						FIOSET4 = MASK_GREEN_4;
					}

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
			}
		}
}

void TFT_Print (uint8_t *stg, uint8_t x, uint8_t y,uint8_t tamanio, uint8_t color)
{
	uint8_t i=0,k;

	while (*stg)
	{
		TFT_PrintChar((*stg),x+TAMANIO_X(tamanio)*i,y,tamanio,color);
		stg++;
		i++;
	}
}
*/

/**
 * \fn void TFT_Print (uint8_t *stg, uint8_t x, uint16_t y,uint8_t tamanio, uint8_t color)
 * \brief Imprime en la TFT el stg
 * \details Imprime en la TFT el stg
 * \param [in] stg String a imprimir
 * \param [in] x Posicion en X
 * \param [in] y posicion en Y
 * \param [in] tamanio Tamaño de la fuente: BIG_FONT o SMALL_FONT
 * \param [in] Color Color
 */
void TFT_Print (uint8_t *stg, uint8_t x, uint16_t y,uint8_t tamanio, uint8_t color)
{
	uint8_t i,j,k=0,l,*ch;
	uint8_t * Bufer[30];

	uint8_t len =strlen((char*)stg);

	TFT_Set_Address(x,y, x + (TAMANIO_X(tamanio)*(len)) -1, y+ TAMANIO_Y(tamanio)-1 );

	ch= (tamanio)?Big_Font:Small_Font ;

	while ((*stg))
	{
		Bufer[k] = ch + ((*stg)-'!'+1)*12;
		k++; stg++;
	}
	Bufer[k]=ch;
	Bufer[k+1]=ch;

	for (k=0;k<16;k++)
	{
		ClrPin(DB[k][0],DB[k][1]);
	}

	SetPin(TFT_RS);


	if (color==WHITE)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for (l=0;l<(len);l++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{

					if( (*(Bufer[l])) & ( 1<< (7-i) ) )
					{
						FIOSET1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
						FIOSET2 = MASK_BLUE_2;
						FIOSET4 = MASK_GREEN_4;

					}
					else
					{
						FIOCLR1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
						FIOCLR2 = MASK_BLUE_2;
						FIOCLR4 = MASK_GREEN_4;
					}

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
				Bufer[l]++;
			}

		}

	}

	if (color==BLACK)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for (l=0;l<(len);l++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{

					FIOCLR1 = (MASK_RED_1) | (MASK_GREEN_1) | (MASK_BLUE_1);
					FIOCLR2 = MASK_BLUE_2;
					FIOCLR4 = MASK_GREEN_4;

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
				Bufer[l]++;
			}

		}

	}


	if (color==RED)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for (l=0;l<(len);l++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{

					if( (*(Bufer[l])) & ( 1<< (7-i) ) )
					{
						FIOSET1 = (MASK_RED_1);
					}
					else
					{
						FIOCLR1 = (MASK_RED_1);
					}

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
				Bufer[l]++;
			}

		}

	}

	if (color==BLUE)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for (l=0;l<(len);l++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{

					if( (*(Bufer[l])) & ( 1<< (7-i) ) )
					{
						FIOSET1 = (MASK_BLUE_1);
						FIOSET2 = MASK_BLUE_2;
					}
					else
					{
						FIOCLR1 = (MASK_BLUE_1);
						FIOCLR2 = MASK_BLUE_2;
					}

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
				Bufer[l]++;
			}

		}

	}

	if (color==GREEN)
	{
		for( j=0 ; j < (TAMANIO_Y(tamanio)) ; j++)
		{
			for (l=0;l<(len);l++)
			{
				for(i=0;i<(TAMANIO_X(tamanio));i++)
				{

					if( (*(Bufer[l])) & ( 1<< (7-i) ) )
					{
						FIOSET1 = (MASK_GREEN_1);
						FIOSET4 = MASK_GREEN_4;

					}
					else
					{
						FIOCLR1 = (MASK_GREEN_1);
						FIOCLR4 = MASK_GREEN_4;
					}

					ClrPin (TFT_WR);
					SetPin (TFT_WR);

				}
				Bufer[l]++;
			}

		}

	}


}

/**
 * \fn void TFT_Three_Lines ( void )
 * \brief Dibuja 3 lineas en la pantalla (RGB)
 * \details No esta en uso
 */
void TFT_Three_Lines ( void ) //Funcion de prueba de pantallas
{
	int i,j,k;
	uint16_t color = 0;
	TFT_Set_Address(0,0,239,319);

	for (k=0;k<16;k++)
	{
		ClrPin(DB[k][0],DB[k][1]);
	}

    for(i=0;i<320;i++)
    {
	  for (j=0;j<240;j++)
	  {
		  if (j < 80) color = TFT_Color(0xFF,0,0);
		  else if ( j < 160 ) color = TFT_Color (0,0xFF,0);
		  else if (j>160) color = TFT_Color(0,0,0xFF);

		  TFT_Send_Color(color);
	  }
    }
}
