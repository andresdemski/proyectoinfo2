/*
 * Touch.c
 *
 *  Created on: 18/09/2013
 *      Author: andresdemski
 */

#include "MyLPC1769.h"
#include "port.h"
#include "ssp.h"
#include "InfotronicKit.h"
#include "Touch.h"

#define PixSizeX	13.78
#define PixOffsX	411
#define PixSizeY	11.01
#define PixOffsY	378

TouchCord_ TouchXY;

/**
 * \fn void Init_Touch (void)
 * \brief Inicializa el Touch
 * \details Inicializa el Touch
 */
void Init_Touch (void)
{
	SetPinSel(TOUCH_PENIRQ,0);
	SetPinDir(TOUCH_PENIRQ,ENTRADA);
	SetPinMode(TOUCH_PENIRQ,PULLUP);
	SetPin (TOUCH_PENIRQ);

	SetPinSel(TOUCH_CS,0);
	SetPinDir(TOUCH_CS,SALIDA);
	SetPinModeOD(TOUCH_CS,O_NORMAL);
	SetPin (TOUCH_CS);

	//Get_Touch();
/*
	SetPinSel(TOUCH_BUSY,0);
	SetPinDir(TOUCH_BUSY,ENTRADA);
	SetPinMode(TOUCH_BUSY,PULLDOWN);
*/
	Get_Touch();
}

/**
 * \fn void Get_Touch (void)
 * \brief Obtiene informacion del Touch
 * \details La informacion la deja en la variable global TOUCHXY
 */

void Get_Touch (void)
{
	TouchXY.X=0;
	TouchXY.Y =0;

	uint16_t X=0,Y=0;
	uint8_t txBuf=0x90,rxBuf[2]={0,0};
	SSP_DATA_SETUP_Type data;

	ClrPin(TOUCH_CS);

			data.length=1;
			data.rx_data=NULL;
			data.tx_data=&txBuf;
			SSP_ReadWrite(&SSP1,&data);

			data.rx_data= rxBuf;
			data.length=2;
			data.tx_data=NULL;
			SSP_ReadWrite(&SSP1,&data);

	Y= ((rxBuf[0]<<8)|rxBuf[1])>>3;

			txBuf=0xD0;
			data.length=1;
			data.rx_data=NULL;
			data.tx_data=&txBuf;
			SSP_ReadWrite(&SSP1,&data);

			data.rx_data= rxBuf;
			data.length=2;
			data.tx_data=NULL;
			SSP_ReadWrite(&SSP1,&data);

	X= ( (rxBuf[0]<<8)|rxBuf[1])>>3;

			txBuf=0x80;  //Dejo preparado para proximas solicitudes (permito que me interrumpa al precionar la pantalla)
			data.length=1;
			data.rx_data=NULL;
			data.tx_data=&txBuf;
			SSP_ReadWrite(&SSP1,&data);

	SetPin(TOUCH_CS);

	X= ( (X - 4371) /14);
	if (X>240) X=240;
	TouchXY.X = X;
	Y = ( (Y - 4371) /10);
	if (Y>320) Y=320;
	TouchXY.Y = Y;


}
