/*
 * variables.h
 *
 *  Created on: 25/09/2013
 *      Author: andresdemski
 */

#ifndef VARIABLES_H_
#define VARIABLES_H_

///////////TOUCH///////////
// Flag: TOUCH_FLAG: Datos adquiridos
// Flag: TOUCH_GET_FLAG: Listo para la conversion

extern uint16_t TOUCH_X;
extern uint16_t TOUCH_Y;

/////// Archivo que se esta reproduciendo//////
extern uint8_t 	File_Selected;
extern uint8_t  File_Before;
extern uint8_t  File_Playing;

#endif /* VARIABLES_H_ */
