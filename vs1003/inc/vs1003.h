 /*
 * vs1003.h
 *
 *  Created on: 03/09/2013
 *      Author: Andres
 */

#ifndef VS1003_H_
#define VS1003_H_

 // SCI registers //

#define SCI_MODE        0x00
#define SCI_STATUS      0x01
#define SCI_BASS        0x02
#define SCI_CLOCKF      0x03
#define SCI_DECODE_TIME 0x04
#define SCI_AUDATA      0x05
#define SCI_WRAM        0x06
#define SCI_WRAMADDR    0x07
#define SCI_HDAT0       0x08  // VS1063, VS1053, VS1033, VS1003, VS1011
#define SCI_HDAT1       0x09  // VS1063, VS1053, VS1033, VS1003, VS1011
#define SCI_AIADDR      0x0A
#define SCI_VOL         0x0B
#define SCI_AICTRL0     0x0C  // VS1063, VS1053, VS1033, VS1003, VS1011
#define SCI_AICTRL1     0x0D  // VS1063, VS1053, VS1033, VS1003, VS1011
#define SCI_AICTRL2     0x0E
#define SCI_AICTRL3     0x0F


// SCI register recording aliases

#define SCI_RECQUALITY 0x07  // (WRAMADDR) VS1063
#define SCI_RECDATA    0x08  // (HDAT0)    VS1063
#define SCI_RECWORDS   0x09  // (HDAT1)    VS1063
#define SCI_RECRATE    0x0C  // (AICTRL0)  VS1063, VS1053
#define SCI_RECDIV     0x0C  // (AICTRL0)  VS1033, VS1003
#define SCI_RECGAIN    0x0D  // (AICTRL1)  VS1063, VS1053, VS1033, VS1003
#define SCI_RECMAXAUTO 0x0E  // (AICTRL2)  VS1063, VS1053, VS1033
#define SCI_RECMODE    0x0F  // (AICTRL3)  VS1063, VS1053

 // SCI_MODE bits

#define SM_DIFF_B             0
#define SM_RESET_B            2
#define SM_OUTOFWAV_B         3  // VS1033, VS1003, VS1011
#define SM_PDOWN_B            4  // VS1003, VS1103
#define SM_TESTS_B            5
#define SM_STREAM_B           6  // VS1053, VS1033, VS1003, VS1011
#define SM_DACT_B             8
#define SM_SDIORD_B           9
#define SM_SDISHARE_B        10
#define SM_SDINEW_B          11
#define SM_ADPCM_B           12  // VS1053, VS1033, VS1003
#define SM_ADPCM_HP_B        13  // VS1033, VS1003
#define SM_LINE_IN_B         14  // VS1033, VS1003, VS1103

#define SM_DIFF           (1<< 0)
#define SM_RESET          (1<< 2)
#define SM_OUTOFWAV       (1<< 3)  // VS1033, VS1003, VS1011
#define SM_PDOWN          (1<< 4)  // VS1003, VS1103
#define SM_TESTS          (1<< 5)
#define SM_STREAM         (1<< 6)  // VS1053, VS1033, VS1003, VS1011
#define SM_DACT           (1<< 8)
#define SM_SDIORD         (1<< 9)
#define SM_SDISHARE       (1<<10)
#define SM_SDINEW         (1<<11)
#define SM_ADPCM          (1<<12)  // VS1053, VS1033, VS1003
#define SM_ADPCM_HP       (1<<13)  // VS1033, VS1003
#define SM_LINE_IN        (1<<14)  // VS1033, VS1003, VS1103

#define SM_ICONF_BITS 2
#define SM_ICONF_MASK 0x00c0

#define SM_EARSPEAKER_1103_BITS 2
#define SM_EARSPEAKER_1103_MASK 0x3000


 // SCI_STATUS bits

#define SS_APDOWN1_B        2
#define SS_APDOWN2_B        3
#define SS_VER_B            4

#define SS_APDOWN1       (1<< 2)
#define SS_APDOWN2       (1<< 3)
#define SS_VER           (1<< 4)

#define SS_SWING_BITS     3
#define SS_SWING_MASK     0x7000
#define SS_VER_BITS       4
#define SS_VER_MASK       0x00f0
#define SS_AVOL_BITS      2
#define SS_AVOL_MASK      0x0003

#define SS_VER_VS1001 0x00
#define SS_VER_VS1011 0x10
#define SS_VER_VS1002 0x20
#define SS_VER_VS1003 0x30
#define SS_VER_VS1053 0x40
#define SS_VER_VS8053 0x40
#define SS_VER_VS1033 0x50
#define SS_VER_VS1063 0x60
#define SS_VER_VS1103 0x70


 // SCI_BASS bits

#define ST_AMPLITUDE_B 12
#define ST_FREQLIMIT_B  8
#define SB_AMPLITUDE_B  4
#define SB_FREQLIMIT_B  0

#define ST_AMPLITUDE (1<<12)
#define ST_FREQLIMIT (1<< 8)
#define SB_AMPLITUDE (1<< 4)
#define SB_FREQLIMIT (1<< 0)

#define ST_AMPLITUDE_BITS 4
#define ST_AMPLITUDE_MASK 0xf000
#define ST_FREQLIMIT_BITS 4
#define ST_FREQLIMIT_MASK 0x0f00
#define SB_AMPLITUDE_BITS 4
#define SB_AMPLITUDE_MASK 0x00f0
#define SB_FREQLIMIT_BITS 4
#define SB_FREQLIMIT_MASK 0x000f

 // SCI_CLOCKF bits

#define SC_MULT_B 13  // VS1063, VS1053, VS1033, VS1103, VS1003
#define SC_ADD_B  11  // VS1063, VS1053, VS1033, VS1003
#define SC_FREQ_B  0  // VS1063, VS1053, VS1033, VS1103, VS1003

#define SC_MULT (1<<13)  // VS1063, VS1053, VS1033, VS1103, VS1003
#define SC_ADD  (1<<11)  // VS1063, VS1053, VS1033, VS1003
#define SC_FREQ (1<< 0)  // VS1063, VS1053, VS1033, VS1103, VS1003

#define SC_MULT_BITS 3
#define SC_MULT_MASK 0xe000
#define SC_ADD_BITS 2
#define SC_ADD_MASK  0x1800
#define SC_FREQ_BITS 11
#define SC_FREQ_MASK 0x07ff

 // The following macro is for VS1063, VS1053, VS1033, VS1003, VS1103.
//   Divide hz by two when calling if SM_CLK_RANGE = 1

#define HZ_TO_SC_FREQ(hz) (((hz)-8000000+2000)/4000)

 // Following are for VS1003 and VS1033
#define SC_MULT_03_10X 0x0000
#define SC_MULT_03_15X 0x2000
#define SC_MULT_03_20X 0x4000
#define SC_MULT_03_25X 0x6000
#define SC_MULT_03_30X 0x8000
#define SC_MULT_03_35X 0xa000
#define SC_MULT_03_40X 0xc000
#define SC_MULT_03_45X 0xe000

 // Following are for VS1003 and VS1033
#define SC_ADD_03_00X 0x0000
#define SC_ADD_03_05X 0x0800
#define SC_ADD_03_10X 0x1000
#define SC_ADD_03_15X 0x1800


 // SCI_WRAMADDR bits

#define SCI_WRAM_X_START           0x0000
#define SCI_WRAM_Y_START           0x4000
#define SCI_WRAM_I_START           0x8000
#define SCI_WRAM_IO_START          0xC000
#define SCI_WRAM_PARAMETRIC_START  0xC0C0  // VS1063
#define SCI_WRAM_Y2_START          0xE000  // VS1063

#define SCI_WRAM_X_OFFSET  0x0000
#define SCI_WRAM_Y_OFFSET  0x4000
#define SCI_WRAM_I_OFFSET  0x8000
#define SCI_WRAM_IO_OFFSET 0x0000  // I/O addresses are @0xC000 -> no offset
#define SCI_WRAM_PARAMETRIC_OFFSET (0xC0C0-0x1E00)  // VS1063
#define SCI_WRAM_Y2_OFFSET 0x0000                   // VS1063


 // SCI_VOL bits

#define SV_LEFT_B  8
#define SV_RIGHT_B 0

#define SV_LEFT  (1<<8)
#define SV_RIGHT (1<<0)

#define SV_LEFT_BITS  8
#define SV_LEFT_MASK  0xFF00
#define SV_RIGHT_BITS 8
#define SV_RIGHT_MASK 0x00FF


//PINS
#define		xRST		Expantion18
#define		DREQ		Expantion10
#define		xCS			Expantion0
#define		xDCS		SSEL

void InitVS1003 (void);
void WriteSci (uint8_t addr, uint16_t data);
uint16_t ReadSci (uint8_t addr);
void WriteSdi (uint8_t *data, uint16_t bytes);



#endif  // VS1003_H_
