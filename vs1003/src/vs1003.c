/*
 * vs1003.c
 *
 *  Created on: 05/09/2013
 *      Author: andresdemski
 */

#include "MyLPC1769.h"
#include "InfotronicKit.h"
#include "port.h"
#include "delay.h"
#include "ssp.h"
#include "vs1003.h"

void InitVS1003 (void)
{
	uint32_t i=0;
	uint8_t limpiar=0;

	SetPinSel(xRST,0);
	SetPinDir(xRST,SALIDA);
	SetPinModeOD(xRST,O_NORMAL);

	SetPin (xRST);
	ClrPin (xRST);

	SetPinSel(DREQ,0);
	SetPinDir(DREQ,ENTRADA);
    SetPinModeOD(DREQ,PULLUP);

	SetPinSel(xDCS,0);
	SetPinDir(xDCS,SALIDA);
	SetPinModeOD(xDCS,O_NORMAL);
	SetPin (xDCS);

	SetPinSel(xCS,0);
	SetPinDir(xCS,SALIDA);
	SetPinModeOD(xCS,O_NORMAL);
	SetPin (xCS);

	SetPin(xRST);
	msDelay(100);

	WriteSci(SCI_MODE,SM_SDINEW|SM_RESET);
	//wait (100)
	WriteSci(SCI_CLOCKF, HZ_TO_SC_FREQ(12288000) | SC_MULT_03_30X | SC_ADD_03_10X);
	WriteSci(SCI_AUDATA, 0x1F40);

	setSSPclock(&SSP1,2500000);
	WriteSci(SCI_VOL, 0x0);
	WriteSci(SCI_DECODE_TIME,0);

	for (i=0;i<1250;i++)
	{
		WriteSdi(&limpiar,1);
	}
}

void WriteSci (uint8_t addr, uint16_t data)
{
	SSP_DATA_SETUP_Type SCI;
	uint8_t rxBuf=0,txBuf=0;
	SCI.rx_data = &rxBuf;
	SCI.length=1;
	SCI.tx_data = &txBuf;

	while (! GetPin(DREQ));

	ClrPin(xCS);

	txBuf=2; //Escribo
	SSP_ReadWrite(&SSP1,&SCI);

	txBuf=addr; //Donde
	SSP_ReadWrite(&SSP1,&SCI);

	txBuf= (data>>8); //Primero
	SSP_ReadWrite(&SSP1,&SCI);

	txBuf= (data&0xFF); //Segundo
	SSP_ReadWrite(&SSP1,&SCI);

	SetPin(xCS);
}

uint16_t ReadSci (uint8_t addr)
{
	SSP_DATA_SETUP_Type SCI;
	uint8_t rxBuf[2]={0,0}, txBuf=0;
	SCI.rx_data = &rxBuf;
	SCI.length=1;
	SCI.tx_data = &txBuf;
	uint16_t res=0;

	while (! GetPin(DREQ));
	ClrPin(xCS);

	txBuf=3;  //Comando lectura
	SSP_ReadWrite(&SSP1,&SCI);

	txBuf=addr; //Que registro
	SSP_ReadWrite(&SSP1,&SCI);

	SCI.length=2;
	SCI.tx_data=NULL;
	SSP_ReadWrite(&SSP1,&SCI); //Leo
	if ( SCI.rx_cnt == 2) res = rxBuf[0]<<8 | rxBuf[1];

	SetPin (xCS);
	return res;
}

void WriteSdi (uint8_t *data, uint16_t bytes)
{
	SSP_DATA_SETUP_Type SDI;
	SDI.rx_data = NULL;
	SDI.length=bytes;
	SDI.tx_data = data;

	while (! GetPin(DREQ));

	ClrPin(xDCS);

	SSP_ReadWrite(&SSP1,&SDI);

	SetPin(xDCS);
}

